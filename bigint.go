package bigint

type Bigint string

func NewInt(num string) (Bigint, error) {
	number, err := checker(num)
	return Bigint(number), err
}
