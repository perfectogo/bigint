package bigint

import (
	"fmt"
	"strconv"
	"strings"
)

func Add(num1, num2 Bigint) Bigint {
	var (
		result    string
		n1, n2, r int
	)
	nums1, nums2, _ := SliceMaker(num1, num2)
	for i := len(nums1) - 1; i >= -1; i-- {
		n1, n2 = 0, 0
		if i >= 0 {
			n1, _ = strconv.Atoi(nums1[i])
			n2, _ = strconv.Atoi(nums2[i])
		}

		res := n1 + n2 + r
		r = 0
		if res > 9 {
			r = res / 10
			result = fmt.Sprint(res%10) + result
			continue
		}
		result = fmt.Sprint(res) + result
	}
	for result[0] == '0' && len(result) != 1 {
		result = strings.Replace(result, "0", "", 1)
	}
	return Bigint(result)
}

func Sub(num1, num2 Bigint) Bigint {
	var (
		result string
		n1, n2 int
		idf    bool
	)
	nums1, nums2, isReversed := SliceMaker(num1, num2)
	for i := len(nums1) - 1; i >= -1; i-- {
		n1, n2 = 0, 0
		if i >= 0 {
			n1, _ = strconv.Atoi(nums1[i])
			n2, _ = strconv.Atoi(nums2[i])
		}
		if idf {
			n1 -= 1
		}
		if n1 < n2 {
			idf = true
			result = fmt.Sprint(n1+10-n2) + result
		}

		if n1 >= n2 {
			idf = false
			result = fmt.Sprint(n1-n2) + result
		}

	}

	for result[0] == '0' && len(result) != 1 {
		result = strings.Replace(result, "0", "", 1)
	}

	if isReversed {
		result = "-" + result
	}
	return Bigint(result)

}

func Mult(a, b Bigint) Bigint {
	return Bigint("")
}

func Mod(a, b Bigint) Bigint {
	return Bigint("")
}
