package bigint

import (
	"reflect"
	"testing"
)

func TestNewInt(t *testing.T) {
	tests := []struct {
		name    string
		input   string
		output  Bigint
		wantErr bool
	}{
		{
			name:    "successful",
			input:   "0",
			output:  Bigint("0"),
			wantErr: true,
		},
		{
			name:    "successful",
			input:   "1234",
			output:  Bigint("1234"),
			wantErr: true,
		},
		{
			name:    "successful",
			input:   "1_000_000_000_000",
			output:  Bigint("1000000000000"),
			wantErr: true,
		}}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got, err := NewInt(tc.input)
			if err != nil {
				t.Fatalf("%s: expected: %v, got: %v", tc.name, tc.wantErr, err)
			}
			if !reflect.DeepEqual(tc.output, got) {
				t.Fatalf("%s: expected: %v, got: %v", tc.name, tc.output, got)
			}
		})
	}
}
