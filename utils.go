package bigint

import (
	"errors"
	"strconv"
	"strings"
)

func checker(number string) (string, error) {
	var err error
	number = strings.NewReplacer("_", "", " ", "").Replace(number)
	for i, ch := range number {
		if i == 0 && string(ch) == "-" {
			if number[i+1] == 48 && len(number) != 1 {
				err = errors.New("number mustn't start with zero: " + string(ch))
				return "", err
			}
			continue
		}
		if 48 > ch || ch > 57 {
			err = errors.New("must only enter number!. you can't use: " + string(ch))
			return "", err
		}
		if i == 0 && ch == 48 && len(number) != 1 {
			err = errors.New("number mustn't start with zero: " + string(ch))
			return "", err
		}
	}

	return number, nil
}

func SliceMaker(num1, num2 Bigint) (nums1, nums2 []string, isReversed bool) {
	idf := true
	if len(num1) > len(num2) {
		idf = false

	}
	if len(num1) < len(num2) {
		idf = false
		isReversed = true
		num1, num2 = num2, num1
	}
	for len(num2) != len(num1) {
		num2 = "0" + num2
	}

	nums1 = strings.Split(string(num1), "")
	nums2 = strings.Split(string(num2), "")

	if !isReversed && idf {
		for i, n := range nums1 {
			n1, _ := strconv.Atoi(n)
			n2, _ := strconv.Atoi(nums2[i])

			if n1 < n2 {
				nums1, nums2 = nums2, nums1
				isReversed = true
				break
			}
		}
	}
	return nums1, nums2, isReversed
}
