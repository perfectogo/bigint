package bigint

import (
	"reflect"
	"testing"
)

func TestAdd(t *testing.T) {
	tests := []struct {
		name    string
		input1  Bigint
		input2  Bigint
		output  Bigint
		wantErr bool
	}{
		{
			name:    "successful",
			input1:  Bigint("4"),
			input2:  Bigint("5"),
			output:  Bigint("9"),
			wantErr: true,
		},
		{
			name:    "successful",
			input1:  Bigint("4"),
			input2:  Bigint("3"),
			output:  Bigint("6"),
			wantErr: false,
		},
		{
			name:    "successful",
			input1:  Bigint("4093873"),
			input2:  Bigint("377390238"),
			output:  Bigint("381484111"),
			wantErr: true,
		},
		{
			name:    "successful",
			input1:  Bigint("93"),
			input2:  Bigint("339384834934"),
			output:  Bigint("339384835027"),
			wantErr: true,
		},
		{
			name:    "successful",
			input1:  Bigint("1000000000000000000001"),
			input2:  Bigint("10000000000000000000000000000000000001"),
			output:  Bigint("10000000000000001000000000000000000002"),
			wantErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := Add(tc.input1, tc.input2)
			if reflect.DeepEqual(tc.output, got) != tc.wantErr {
				t.Fatalf("%s: expected: %v, got: %v", tc.name, tc.output, got)
			}
		})
	}
}
func TestSub(t *testing.T) {
	tests := []struct {
		name    string
		input1  Bigint
		input2  Bigint
		output  Bigint
		wantErr bool
	}{
		{
			name:    "successful",
			input1:  Bigint("4"),
			input2:  Bigint("4"),
			output:  Bigint("0"),
			wantErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := Sub(tc.input1, tc.input2)
			if reflect.DeepEqual(tc.output, got) != tc.wantErr {
				t.Fatalf("%s: expected: %v, got: %v", tc.name, tc.output, got)
			}
		})
	}
}
