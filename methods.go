package bigint

import "strings"

func (z *Bigint) Set(num string) error {
	number, err := checker(num)
	*z = Bigint(number)
	return err
}

func (x *Bigint) Abs() Bigint {
	var str string = string(*x)
	str = strings.Replace(str, "-", "", 1)
	return Bigint(str)
}
