package main

import (
	"fmt"
	"log"

	"gitlab.com/perfectogo/bigint"
)

func main() {
	number1, err := bigint.NewInt("1")
	if err != nil {
		log.Fatal(err)
	}
	number2, err := bigint.NewInt("1")
	if err != nil {
		log.Fatal(err)
	}
	result := bigint.Add(number1, number2)

	fmt.Println(result)
}
